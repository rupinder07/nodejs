const mongoose = require('mongoose');
const logger = require('./logger');

const url = process.env.MONGOURL;
mongoose.Promise = global.Promise;

mongoose.connect(url, error => {
  if (error) {
    logger.info(`Mongoose default connection error: ${error}`);
  } else {
    logger.info('mongo Connected :)');
  }
});
// Create the database connection
// When successfully connected
mongoose.connection.on('connected', () => {
  logger.info(`Mongoose default connection open to ${url}`);
});

// If the connection throws an error
mongoose.connection.on('error', err => {
  logger.info(`Mongoose default connection error: ${err}`);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', () => {
  logger.info('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    logger.info('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

module.exports = mongoose;
