const log = require('loglevel');

log.setLevel(global.configuration.logLevel);
const logger = log;
// logger.info = (msg) => {}; /** uncomment to prevent spamming terminal with sql queries */
module.exports = logger;
