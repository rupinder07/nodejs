// Middleware for Application
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const CsrfMiddleware = require('./global/middlewares/csrfMiddleware');
const EmptyContentMiddleware = require('./global/middlewares/emptyContent');
const ContentTypeMiddleware = require('./global/middlewares/contentType');
const ErrorHanlder = require('./global/middlewares/errorHandler');

const middleware = app => {
  app.use(passport.initialize());
  app.set('port', process.env.PORT || '3000');
  // adding security fixes
  app.disable('x-powered-by');
  app.use(helmet());
  app.use(helmet.noCache({ noEtag: true })); // set Cache-Control header
  app.use(helmet.noSniff()); // set X-Content-Type-Options header
  app.use(helmet.frameguard()); // set X-Frame-Options header
  app.use(helmet.xssFilter()); // set X-XSS-Protection header

  app.enable('trust proxy', ['loopback', 'linklocal', 'uniquelocal']);

  app.use(
    bodyParser.urlencoded({
      extended: false,
    }),
  ); // parse application/x-www-form-urlencoded
  app.use(bodyParser.json()); // parse application/json
  /**
   * enable CORS support. // Cross-Origin Request Support
   */
  // register all custom Middleware
  app.use(
    cors({
      optionsSuccessStatus: 200,
    }),
  );
  app.use(ContentTypeMiddleware);
  app.use(EmptyContentMiddleware);
  app.use(CsrfMiddleware);
};

module.exports = middleware;
