class Utility {
  static parseToJSONObj(json) {
    if (typeof json !== 'string' && typeof json === 'object') {
      return json;
    }
    try {
      return JSON.parse(json);
    } catch (e) {
      return e;
    }
  }
}

module.exports = Utility;
