const UserModel = require('../services/user/model/user');
const UserController = require('../services/user/controller/userController');
const UserTransformer = require('../services/user/transformer/userTransformer');

class UserService {
  static findById(id, callback) {
    UserModel.findById(id, (error, user) => {
      if (error) {
        callback(error);
      } else {
        callback(null, user);
      }
    });
  }

  static findOne(options, callback) {
    UserModel.findOne(options, (error, user) => {
      if (error) {
        callback(error);
      } else {
        callback(null, user);
      }
    });
  }

  static registerSocial(user, done) {
    return UserController.registerSocial(user, done);
  }

  static transform(users) {
    return UserTransformer.transform(users);
  }
}

module.exports = {
  user: UserService,
};
