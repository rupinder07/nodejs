/* eslint func-names : 0 */
/* eslint no-param-reassign:0 */
const _ = require('lodash');
const Bluebird = require('bluebird');
const util = require('util');
const jwt = require('jsonwebtoken');
const serverConfig = require('../config/server');

Bluebird.promisifyAll(jwt);

function JWTServiceError(error) {
  Error.call(this);
  this.message = 'JWTServiceError';
  this.error = error;
}

function UnauthorizedAccessError(error) {
  Error.call(this);
  this.message = 'UnauthorizedAccessError';
  this.error = error || 'Token verification failed. Either token already expired or inavlid token provided';
}

function NoTokenProvidedError(error) {
  Error.call(this);
  this.message = 'NoTokenProvidedError';
  this.error = error || 'No Authentication token provided. Please add auth token in Http Headers';
  this.code = 1400;
}
function AuthUserDataError(error) {
  Error.call(this);
  this.message = 'AuthUserDataError';
  this.error = error || 'Unable to fetch Data for loggedIn user please try again';
}

util.inherits(JWTServiceError, Error);
util.inherits(UnauthorizedAccessError, Error);
util.inherits(NoTokenProvidedError, Error);

function JWTRedisService(config) {
  this.issuer = 'Admin';
  this.secret = config.secret;
  this.keyspace = config.keyspace;
  this.expiration = 86400000 / 1000;
}

JWTRedisService.prototype.sign = function(data) {
  const token = jwt.sign({ data }, this.secret, {
    issuer: this.issuer,
    expiresIn: this.expiration,
  });
  return Promise.resolve(token);
};

JWTRedisService.prototype.verify = function(token) {
  return new Promise((resolve, reject) => {
    if (_.isEmpty(token)) {
      reject(new NoTokenProvidedError());
    }
    return (
      jwt
        .verifyAsync(token, this.secret)
        .then(decoded => {
          if (_.isEmpty(decoded.data)) {
            reject(new UnauthorizedAccessError());
          }
          const tokenId = token.split('.')[2];
          if (!tokenId) {
            reject(new UnauthorizedAccessError());
          }
          resolve(decoded.data);
        })
        // eslint-disable-next-line no-unused-vars
        .catch(error => {
          reject(new UnauthorizedAccessError(error));
        })
    );
  });
};
JWTRedisService.prototype.expire = function(token) {
  return new Promise((resolve, reject) => {
    if (_.isEmpty(token)) {
      reject(new NoTokenProvidedError());
    }
    const userData = jwt.decode(token, this.secret);
    if (_.isEmpty(userData) || _.isEmpty(userData.data)) {
      reject(new UnauthorizedAccessError());
    }
    // put check here for not null
    const tokenId = token.split('.')[2];
    if (!tokenId) {
      reject(new UnauthorizedAccessError());
    }
  });
};
JWTRedisService.prototype.JWTServiceError = JWTServiceError;
JWTRedisService.prototype.UnauthorizedAccessError = UnauthorizedAccessError;
JWTRedisService.prototype.NoTokenProvidedError = NoTokenProvidedError;
JWTRedisService.prototype.AuthUserDataError = AuthUserDataError;

const jwtRedisService = new JWTRedisService({
  secret: serverConfig.WEB_TOKEN_SECRET,
});

module.exports = jwtRedisService;
