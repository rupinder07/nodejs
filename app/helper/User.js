const bcrypt = require('bcrypt-nodejs');
const Promise = require('bluebird');
const serverConfig = require('../config/server');
const jwtRedisService = require('./jwt-helper');

class UserHelper {
  static resource(path) {
    return `${serverConfig.HOST}${serverConfig.PORT ? `:${serverConfig.PORT}` : ''}${path}`;
  }

  static comparePassword(userPassword, password) {
    if (!userPassword.length) {
      return false;
    }
    return bcrypt.compareSync(userPassword, password);
  }

  static hashPassword(password) {
    if (password) {
      const salt = bcrypt.genSaltSync(); // enter number of rounds, default: 10
      const hash = bcrypt.hashSync(password, salt);
      return hash;
    }
    return null;
  }

  static sign(data) {
    return jwtRedisService.sign(data);
  }

  static generateToken(user) {
    return new Promise((resolve, reject) =>
      this.sign({
        id: user.id,
        role: user.role,
        email: user.email,
        username: user.username,
      }).then(token => resolve(token), err => reject(err)),
    );
  }
}

module.exports = UserHelper;
