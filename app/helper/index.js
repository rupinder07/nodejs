const fs = require('fs');
const path = require('path');
const configServer = require('../config/server');

const Default = {
  resource(filePath) {
    return `${configServer.HOST}${configServer.PORT ? `:${configServer.PORT}` : ''}${filePath}`;
  },
  getFileExtension(file) {
    const extensions = file.split('.');
    if (extensions.length === 1) {
      return 'jpg';
    }
    return extensions.pop();
  },
  avatarURL(filename) {
    if (filename.includes('://')) {
      return filename;
    }
    return Default.resource(`/${configServer.UPLOAD_DIR}/${configServer.PROFILE_PICTURE_DIR}/${filename}`);
  },
  randomString() {
    return Math.random()
      .toString(36)
      .substring(2, 7);
  },
  deleteFile(type, filename) {
    let location;
    if (type === 'profile') {
      location = path.join(configServer.UPLOAD_DIR, configServer.PROFILE_PICTURE_DIR);
    } else {
      location = configServer.UPLOAD_DIR;
    }

    if (filename) {
      fs.unlink(path.join(location, filename), () => {
        // in case we need to perform additional operations.
      });
    }
  },
};

module.exports = Default;
