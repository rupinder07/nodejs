/* eslint-disable import/no-dynamic-require */
const path = require('path');
require('dotenv').config();

const env = process.env.NODE_ENV || 'dev';
// eslint-disable-next-line no-console
console.log(` using ${process.env.NODE_ENV} to run application`);
global.configuration = require(`../config/environments/${env}`);
const express = require('express');
const AppRoutes = require('./routes');
const logger = require('./lib/logger');
const swagger = require('./swagger');
const ErrorHanlder = require('../app/global/middlewares/errorHandler');

const app = express();
require('./middleware')(app);
require('./lib/mongoose');

swagger(app);
AppRoutes(app);

app.use(express.static(path.join(__dirname, 'public')));
app.use(ErrorHanlder.handleRequestError);

// ---------------------------------------------//
// invoke routes, MIddleware, Mongo connect here

// ---------------------------------------------//
app.listen(process.env.PORT || 3000, () => {
  const port = process.env.PORT || 3000;
  logger.info(`🚀 🚀 🚀 🚀  API Auth Server ready at http://localhost:${port} 🚀 🚀 🚀 `);
});
