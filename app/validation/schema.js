// schemas.js
const Joi = require('joi');

const schemas = {
  login: Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required(),
  }),
  register: Joi.object().keys({
    username: Joi.string().required(),
    name: Joi.string(),
    first_name: Joi.string(),
    last_name: Joi.string(),
    email: Joi.string().required(),
    password: Joi.string().required(),
  }),
  position: Joi.object().keys({
    project_name: Joi.string().required(),
    client_name: Joi.string().required(),
    role: Joi.string().required(),
    technologies: Joi.string().required(),
    job_desc: Joi.string().required(),
    status: Joi.string().required(),
  }),
};
module.exports = schemas;
