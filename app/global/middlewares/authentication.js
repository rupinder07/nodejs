const Service = require('../../helper/service');
const ResponseTemplate = require('../templates/response');
const jwtHelper = require('../../helper/jwt-helper');
const logger = require('../../lib/logger');

const ValidAuthToken = (req, res, next) => {
  const token = req.headers.authorization;
  if (token) {
    jwtHelper
      .verify(token)
      .then(user => {
        Service.user.findById(user.id, error => {
          if (error) {
            res.status(403).json(ResponseTemplate.commonAuthUserDataError(error));
          }
          req.user = user;
          logger.info('message', req.user);
          logger.info('message', 'user data received from authValidateToken middleware');
          next();
        });
      })
      // eslint-disable-next-line no-unused-vars
      .catch(err => {
        logger.info('message', err);
        res.status(400).json(ResponseTemplate.authError());
      });
  } else {
    res.status(400).json(ResponseTemplate.authError());
  }
};

module.exports = ValidAuthToken;
