const ResponseTemplate = require('../templates/response');

const AuthorizeUser = (req, res, next) => {
  if (req.user) {
    if (req.user && req.user.role === 'manager') {
      next();
    } else {
      res.status(400).json(ResponseTemplate.authorizationError());
    }
  } else {
    res.status(400).json(ResponseTemplate.authorizationError());
  }
};

module.exports = AuthorizeUser;
