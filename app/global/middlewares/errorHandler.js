const {
  BAD_REQUEST_STATUS_CODE,
  BAD_REQUEST_STATUS_MESSAGE,
  INTERNAL_SERVER_MESSAGE,
  INTERNAL_SERVER_STATUS_CODE,
  NOT_FOUND_STATUS_CODE,
  NOT_FOUND_STATUS_MESSAGE,
} = require('../../config/constants');

const { BadRequestException, NotFoundException, ServerException, APIError } = require('../../config/customErrors');
const logger = require('../../lib/logger');

const middlewares = {
  handleRequestError(error, req, res, next) {
    switch (true) {
      case error instanceof NotFoundException:
        res.status(NOT_FOUND_STATUS_CODE).json({
          message: error.message,
          error: NOT_FOUND_STATUS_MESSAGE,
          statusCode: NOT_FOUND_STATUS_CODE,
        });
        break;
      case error instanceof APIError:
        res.status(BAD_REQUEST_STATUS_CODE).json({
          success: false,
          error: BAD_REQUEST_STATUS_CODE,
          statusCode: BAD_REQUEST_STATUS_CODE,
          message: error.message,
          code: error.ErrorID,
        });
        break;
      case error instanceof ServerException:
        res.status(INTERNAL_SERVER_STATUS_CODE).json({
          error: INTERNAL_SERVER_MESSAGE,
          message: error.message,
          statusCode: INTERNAL_SERVER_STATUS_CODE,
        });
        break;
      case error instanceof BadRequestException:
        res.status(BAD_REQUEST_STATUS_CODE).json({
          message: error.message,
          error: BAD_REQUEST_STATUS_MESSAGE,
          statusCode: BAD_REQUEST_STATUS_CODE,
        });
        break;
      default:
        res.status(INTERNAL_SERVER_STATUS_CODE).json({
          message: error.message,
          error: INTERNAL_SERVER_MESSAGE,
          statusCode: INTERNAL_SERVER_STATUS_CODE,
        });
      // send email
    }
    logger.info('Request handle error!!', error);
    res.end();
  },
};

module.exports = middlewares;
