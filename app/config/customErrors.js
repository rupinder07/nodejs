// eslint-disable-next-line max-classes-per-file
const BadRequestException = class BadRequestException extends Error {};
const NotFoundException = class NotFoundException extends Error {};
const ServerException = class ServerException extends Error {};
const APIError = class APIError extends Error {};

module.exports = { BadRequestException, NotFoundException, ServerException, APIError };
