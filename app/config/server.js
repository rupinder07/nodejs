module.exports = {
  HOST: 'localhost',
  PORT: process.env.PORT || '3001',
  SESSION_SECRET: process.env.SESSION_SECRET,
  WEB_TOKEN_SECRET: process.env.WEB_TOKEN_SECRET,
  UPLOAD_DIR: 'uploads',
  PROFILE_PICTURE_DIR: 'profile',
};
