// Application routes
const AuthServiceRoutes = require('./services/auth/routes');
const DefaultServiceRoutes = require('./services/default/routes');
const UserServiceRoutes = require('./services/user/routes');
const ValidAuthTokenMiddleware = require('./global/middlewares/authentication');
const PositionServiceRoutes = require('./services/position/routes');
const ApplicationServiceRoutes = require('./services/applications/routes');

const routes = app => {
  app.use('/api/v1/auth', AuthServiceRoutes);
  app.use('/api/v1/user', ValidAuthTokenMiddleware, UserServiceRoutes);
  app.use('/api/v1/health', DefaultServiceRoutes);
  app.use('/api/v1/position', ValidAuthTokenMiddleware, PositionServiceRoutes);
  app.use('/api/v1/application', ValidAuthTokenMiddleware, ApplicationServiceRoutes);
};

module.exports = routes;
