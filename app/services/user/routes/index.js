const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const User = require('../model/user');
const UserTransformer = require('../transformer/userTransformer');
const ResponseTemplate = require('../../../global/templates/response');
const UserController = require('../controller/userController');
const Helper = require('../../../helper');
const configServer = require('../../../config/server');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, path.join(configServer.UPLOAD_DIR, configServer.PROFILE_PICTURE_DIR));
  },
  filename(req, file, cb) {
    const extension = Helper.getFileExtension(file.originalname);
    // cb( null, `${file.fieldname}-${req.user.id}.${extension}` );
    cb(null, `${req.user.id}-${Helper.randomString()}.${extension}`);
  },
});
const upload = multer({ storage });
const router = express.Router();

router.get('/:id', (req, res) => {
  User.findById(req.params.id, (error, user) => {
    if (error) {
      res.json(ResponseTemplate.userNotFound());
      // res.send(error);
    } else {
      res.json({
        code: 200,
        message: 'success',
        user: UserTransformer.transform(user),
      });
    }
  });
});
router.put('/:userId', (req, res) => {
  UserController.update(req.params.userId, req.body, (error, user) => {
    if (error) {
      res.json(ResponseTemplate.updateErrorOccoured(error));
    } else {
      res.json(ResponseTemplate.success('your data has been successfully updated'));
    }
  });
});
// upload users profile picture.
router.post('/upload-profile', upload.single('avatar'), (req, res) => {
  UserController.update(req.params.userId, { profile_picture: req.file.filename }, (error, user) => {
    if (error) {
      res.json(ResponseTemplate.updateErrorOccoured(error));
    } else {
      res.json(
        ResponseTemplate.success('your profile picture has been successfully uploaded', {
          profile_picture: Helper.avatarURL(user.profile_picture),
        }),
      );
      Helper.deleteFile('profile', req.user.profile_picture);
    }
  });
});

module.exports = router;
