/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
const uuidv4 = require('uuid/v4');
const UserTransformer = require('../transformer/userTransformer');
const User = require('../model/user');
const userHelper = require('../../../helper/user');

const UserController = () => {
  function registerDefault(user, callback) {
    const { username, email, password, first_name, last_name } = user;
    if (!username || !email || !password) {
      callback('invalid data provided');
    } else {
      User.find({ email: user.email }, (error, existingUser) => {
        if (existingUser.length !== 0) {
          callback('User with this email address already exists.');
        } else {
          const hashPassword = userHelper.hashPassword(password);
          const newUser = new User({
            provider: 'default',
            username,
            email,
            uuid: uuidv4(),
            first_name,
            last_name,
            password: hashPassword,
            status: 1,
          });
          newUser.save((err, savedUser) => {
            if (err) {
              callback(error);
            }
            callback(null, UserTransformer.transform(savedUser));
            return UserTransformer.transform(savedUser);
          });
        }
      });
    }
  }
  function registerSocial(user, callback) {
    User.findOne({ email: user.email }, (error, existingUser) => {
      if (existingUser) {
        callback(null, UserTransformer.transform(existingUser));
      } else {
        const newUser = new User({
          provider: user.provider,
          name: user.name,
          email: user.email,
          profile_picture: user.profile_picture,
          email_verified: true,
        });
        newUser.save((err, savedUser) => {
          // if (error) {}
          callback(null, UserTransformer.transform(savedUser));
          return UserTransformer.transform(savedUser);
        });
      }
    });
  }
  function update(id, data, callback) {
    // it can be used to assign role to a user as employee or manager
    User.findById(id, (error, user) => {
      if (user) {
        if (data.name) {
          user.name = data.name;
        }
        if (data.profile_picture) {
          user.profile_picture = data.profile_picture;
        }
        // user password update
        if (data.password && data.confirm_password && data.password === data.confirm_password) {
          user.password = userHelper.hashPassword(data.password);
        }
        if (data.address) {
          user.address = data.address;
        }
        // user role update
        if (data.role) {
          user.role = data.role;
        }
        user.save((err, updatedUser) => {
          if (err) {
            callback('error occoured while updating record');
          } else {
            callback(null, updatedUser);
          }
        });
      } else {
        callback('user not found');
      }
    });
  }
  return {
    registerDefault,
    registerSocial,
    update,
  };
};
module.exports = UserController();
