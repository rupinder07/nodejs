const { string } = require('joi');
const mongoose = require('mongoose');
// UserSchema as manager or employee
const UserSchema = mongoose.Schema(
  {
    provider: String,
    username: String,
    role: {
      type: String,
      default: 'employee',
    },
    email: {
      type: String,
      lowercase: true,
      required: true,
    },
    uuid: {
      type: String,
      required: true,
    },
    password: String,
    profile_picture: String,
    email_verified: Boolean,
    social: [],
    address: String,
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  },
);

module.exports = mongoose.model('User', UserSchema);
