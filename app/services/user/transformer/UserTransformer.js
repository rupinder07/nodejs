const _ = require('lodash');
const Helper = require('../../../helper');

const UserTransformer = {
  transform: users => {
    if (Array.isArray(users)) {
      const output = [];
      _.forEach(users, user => {
        output.push(UserTransformer.transformData(user));
      });
      return output;
    }
    return UserTransformer.transformData(users);
  },
  transformData: user => {
    if (!user) {
      return {};
    }
    return {
      id: user._id,
      role: user.role,
      username: user.username,
      email: user.email,
      password: !!user.password,
      address: user.address || '',
      profile_picture: user.profile_picture ? Helper.avatarURL(user.profile_picture) : null,
      resource_url: Helper.resource(`/users/${user._id}`),
    };
  },
};

module.exports = UserTransformer;
