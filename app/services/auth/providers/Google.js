const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const Service = require('../../../helper/service');

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.G_CLIENTID,
      clientSecret: process.env.G_CLIENTSECRET,
      callbackURL: process.env.G_CALLBACK,
      passReqToCallback: true,
    },
    (req, accessToken, refreshToken, profile, done) => {
      const data = profile._json;
      Service.user.registerSocial(
        {
          provider: 'google',
          name: data.displayName,
          email: profile.emails[0].value,
          profile_picture: data.image.url,
          meta: {
            provider: 'google',
            id: data.id,
            accessToken,
          },
        },
        done,
      );
    },
  ),
);

const GoogleRoutes = {
  authenticate: () => {
    return passport.authenticate('google', {
      scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email'],
    });
  },

  callback: () => {
    return passport.authenticate('google', {
      // successRedirect: '/auth/success',
      failureRedirect: '/auth/failed',
    });
  },
};

module.exports = GoogleRoutes;
