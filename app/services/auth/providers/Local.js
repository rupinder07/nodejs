const passport = require('passport');
const LocalStrategy = require('passport-local');
const Service = require('../../../helper/service');
const userHelper = require('../../../helper/user');

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    (req, email, password, done) => {
      Service.user.findOne({ email }, (error, user) => {
        if (error) {
          return done(error);
        }
        if (!user) {
          return done(null, { code: 401, message: 'error', error: 'incorrect email address' });
        }
        if (!userHelper.comparePassword(password, user.password)) {
          return done(null, { code: 401, message: 'error', error: 'incorrect password' });
        }
        if (user && userHelper.comparePassword(password, user.password)) {
          return done(null, Service.user.transform(user));
        }
        return done(null, { code: 401, message: 'error', error: 'incorrect password provided' });
      });
    },
  ),
);
const LocalRoutes = {
  authenticate: () => {
    return passport.authenticate('local', { session: false });
  },
  authenticate_with_callback: () => {
    return passport.authenticate('local', {
      successRedirect: '/auth/success',
      failureRedirect: '/auth/failed',
    });
  },
};
module.exports = LocalRoutes;
