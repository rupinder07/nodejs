const passport = require('passport');
const FacebookStrategy = require('passport-facebook');
const Service = require('../../../helper/service');

passport.use(
  new FacebookStrategy(
    {
      clientID: process.env.F_CLIENTID,
      clientSecret: process.env.F_CLIENTSECRET,
      callbackURL: process.env.F_CALLBACK,
      profileFields: ['id', 'displayName', 'photos', 'email'],
      passReqToCallback: true,
    },
    (req, accessToken, refreshToken, profile, done) => {
      // eslint-disable-next-line no-underscore-dangle
      const data = profile._json;
      Service.user.registerSocial(
        {
          provider: 'facebook',
          name: data.name,
          email: data.email,
          profile_picture: data.picture.data.url,
          meta: {
            provider: 'facebook',
            id: profile.id,
            accessToken,
          },
        },
        done,
      );
    },
  ),
);

const FacebookRoutes = {
  authenticate: () => {
    return passport.authenticate('facebook', { scope: ['email', 'public_profile', 'user_location'] });
  },

  callback: () => {
    return passport.authenticate('facebook', {
      failureRedirect: '/auth/failed',
    });
  },
};

module.exports = FacebookRoutes;
