const express = require('express');
const passport = require('passport');
const FacebookRoutes = require('./providers/facebook');
const GoogleRoutes = require('./providers/google');
const LocalRoutes = require('./providers/local');
const SchemaMiddleware = require('../../validation/joi-validation');
const Helper = require('../../helper/user');
const UserController = require('../user/controller/userController');
const ValidAuthTokenMiddleware = require('../../global/middlewares/authentication');
const Schema = require('../../validation/schema');
const jwtRedisService = require('../../helper/jwt-helper');
const ResponseTemplate = require('../../global/templates/response');

const router = express.Router();
passport.serializeUser((user, done) => {
  done(null, user);
});
passport.deserializeUser((user, done) => {
  done(null, user);
});
/**
 * @api {POST} /auth/success local auth
 * @apiName auth failure
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */
router.get('/success', (req, res) => {
  res.json({ message: 'success', login: true });
});
/**
 * @api {POST} /auth/failed local auth
 * @apiName auth Success
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */
router.get('/failed', (req, res) => {
  res.json({ message: 'failed', login: false });
});

/**
 * @api {POST} /auth/ local auth
 * @apiName local login
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */

router.post('/login', SchemaMiddleware(Schema.login, 'body'), LocalRoutes.authenticate(), (req, res) => {
  if (!req.user.email) {
    res.status(401).json({
      code: 401,
      success: false,
      message: 'invalid cradenatials provided',
    });
  } else {
    Helper.generateToken(req.user).then(token => {
      res.status(200).json({
        code: 200,
        message: 'loggedin successfully',
        success: true,
        token,
        profile: req.user,
      });
    });
  }
});

router.post('/register', SchemaMiddleware(Schema.register, 'body'), (req, res) => {
  UserController.registerDefault(req.body, (error, user) => {
    if (error) {
      res.status(400).json({ code: 400, success: false, message: error });
    } else {
      res.json({
        success: true,
        message: 'registered successfully',
        description: 'registered successfully',
      });
    }
  });
});

// send accessToken to client side
const redirectSocialUser = (req, res) => {
  Helper.generateToken(req.user).then(accessToken => {
    res.redirect(`${url.FE}/validate-auth-token?accessToken=${accessToken}`);
  });
};

/**
 * @api {POST} /auth/login/facebook Social Login
 * @apiName facebook
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */
router.get('/login/facebook', FacebookRoutes.authenticate());
router.get('/callback/facebook', FacebookRoutes.callback(), redirectSocialUser);
/**
 * @api {POST} /auth/login/google Social Login
 * @apiName google
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */

router.get('/login/google', GoogleRoutes.authenticate());
router.get('/callback/google', GoogleRoutes.callback(), redirectSocialUser);

/**
 * @api {POST} /auth/login/twitter Social Login
 * @apiName twitter
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */

/**
 * @api {GET} /auth/validate validate token with Middleware
 * @apiName validate
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */
/**
 * @swagger
 * /auth/validate:
 *   get:
 *     tags:
 *       - auth
 *     description: validate token and return User
 */
router.get('/validate', ValidAuthTokenMiddleware, (req, res) => {
  return res.status(200).json(ResponseTemplate.validtoken());
});

/**
 * @api {GET} /auth/logout user session clear
 * @apiName logout
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */
/**
 * @swagger
 * /auth/logout:
 *   get:
 *     tags:
 *       - auth
 *     description: logout user session
 */

router.get('/logout', ValidAuthTokenMiddleware, (req, res) => {
  const token = req.headers.authorization;
  return jwtRedisService.expire(token).then(
    () => {
      res.status(200).json(ResponseTemplate.logout());
    },
    error => res.status(403).json(ResponseTemplate.commonAuthUserDataError(error)),
  );
});

/**
 * @api {GET} /auth/user return current loggedin user
 * @apiName user
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */
/**
 * @swagger
 * /auth/logout:
 *   get:
 *     tags:
 *       - auth
 *     description: return current loggedin user
 */

router.get('/user', ValidAuthTokenMiddleware, (req, res) => {
  res.json(req.user);
});
/**
 * @api {POST} /auth/reset-password update password sent in Mail
 * @apiName resetPassword
 * @apiGroup Auth
 * @apiSuccess {String} code HTTP status code =  require ( API.
 * @apiSuccess {String} message Message =  require ( API.
 */
router.get('/reset-password/:email', (req, res) => {
  if (!req.params.email) {
    res.status(404).json({
      code: 404,
      message: 'email address not provided',
      success: false,
    });
  } else {
    UserController.resetPassword(req.params.email, (error, success) => {
      if (error) {
        res.status(404).json({ code: 404, success: false, message: 'provided email is not registered with us' });
      } else {
        res.json({
          code: 200,
          success: true,
          message: 'if this email is registered with us, you will receive a password reset email soon.',
        });
      }
    });
  }
});
module.exports = router;
