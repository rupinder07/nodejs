const express = require('express');
const PositionController = require('../controller/positionController');
const SchemaMiddleware = require('../../../validation/joi-validation');
const AuthorizeUser = require('../../../global/middlewares/authorization');

const router = express.Router();
const Schema = require('../../../validation/schema');

router.get('/', PositionController.getAll);
router.get('/:positionId', PositionController.getById);
router.post('/', SchemaMiddleware(Schema.position, 'body'), AuthorizeUser, PositionController.create);
router.put('/', SchemaMiddleware(Schema.position, 'body'), AuthorizeUser, PositionController.update);
router.delete('/:positionId', PositionController.delete);

module.exports = router;
