/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
const errorMessages = require('../../../validation/errorMessage');
const positionService = require('../service/positionService');
const { ServerException } = require('../../../config/customErrors');

const PositionController = {
  getAll: async (req, res, next) => {
    try {
      const data = await positionService.list();
      res.status(200).json(data);
    } catch (err) {
      next(err);
    }
  },
  getById: async (req, res, next) => {
    const { positionId } = req.params;
    try {
      const data = await positionService.getById(positionId);
      res.status(200).json(data);
    } catch (err) {
      next(new ServerException(err));
    }
  },
  create: async (req, res, next) => {
    try {
      const payload = req.body;
      // eslint-disable-next-line no-underscore-dangle
      payload.created_by = req.user.id;
      const data = await positionService.create(payload);
      res.status(200).json(data);
    } catch (err) {
      next(new ServerException(errorMessages.SERVER_ERROR));
    }
  },
  delete: async (req, res, next) => {
    const { positionId } = req.params;
    try {
      const data = await positionService.deletePost(positionId);
      res.status(200).json(data);
    } catch (err) {
      next(new ServerException(errorMessages.SERVER_ERROR));
    }
  },
  update: async (req, res, next) => {
    const positionId = req.params;
    const payload = req.body;
    try {
      const data = await positionService.update(positionId, payload);
      res.status(200).json(data);
    } catch (err) {
      next(new ServerException(errorMessages.SERVER_ERROR));
    }
  },
};

module.exports = PositionController;
