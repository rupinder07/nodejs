const { string } = require('joi');
const mongoose = require('mongoose');
// UserSchema as manager or employee
const PositionSchema = mongoose.Schema(
  {
    project_name: String,
    client_name: String,
    role: String,
    technologies: [],
    job_desc: String,
    status: {
      type: String,
      default: 'active',
    },
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  },
);

module.exports = mongoose.model('Position', PositionSchema);
