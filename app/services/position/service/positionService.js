const Position = require('../model/position');

const PositionService = {
  list: async () => {
    return Position.find({ status: 'active' }).populate('created_by', { email: 1, username: 1 });
  },
  getById: async id => {
    return Position.findById(id).populate('created_by', { email: 1, username: 1 });
  },
  create: async payload => {
    const newPosition = await Position.create(payload);
    return newPosition.save();
  },
  update: async (positionId, payload) => {
    return Position.findByIdAndUpdate(positionId, payload).populate('user');
  },
  deletePost: async postId => {
    return Position.findByIdAndRemove(postId);
  },
};

module.exports = PositionService;
