const express = require('express');
const Helper = require('../../helper');

const router = express.Router();
// default / get route
router.get('/', (req, res) => {
  res.json({
    code: 200,
    message: 'I am alive and listening',
    resources: [
      {
        resource: 'users',
        status: 'in development',
        url: Helper.resource('/api/v1/user'),
      },
      {
        resource: 'auth',
        status: 'in development',
        default: {
          method: 'POST',
          url: Helper.resource('/api/v1/auth'),
        },
        providers: [
          { name: 'facebook', url: Helper.resource('/api/v1/auth/login/facebook') },
          { name: 'google', url: Helper.resource('/api/v1/auth/login/google') },
        ],
      },
    ],
  });
});

module.exports = router;
