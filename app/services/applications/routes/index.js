const express = require('express');
const ApplicationController = require('../controller/applicationsController');
const Authorization = require('../../../global/middlewares/authorization');

const router = express.Router();
// services and routes for job application
// once user apply for job position
router.get('/', Authorization, Authorization, ApplicationController.getAll);
router.get('/:applicationId', ApplicationController.getById);
router.get('/apply/:positionId', ApplicationController.create);

module.exports = router;
