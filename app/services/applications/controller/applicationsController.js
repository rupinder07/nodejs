/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
const errorMessages = require('../../../validation/errorMessage');
const applicationService = require('../service/applicationService');
const { ServerException } = require('../../../config/customErrors');

const PositionController = {
  getAll: async (req, res, next) => {
    try {
      const data = await applicationService.list();
      res.status(200).json(data);
    } catch (err) {
      next(err);
    }
  },
  getById: async (req, res, next) => {
    const { applicationId } = req.params;
    try {
      const data = await applicationService.getById(applicationId);
      res.status(200).json(data);
    } catch (err) {
      next(new ServerException(err));
    }
  },
  create: async (req, res, next) => {
    const { positionId } = req.params;
    try {
      const payload = {};
      payload.user = req.user.id;
      payload.position = positionId;
      console.log(payload);
      // eslint-disable-next-line no-underscore-dangle
      const data = await applicationService.create(payload);
      res.status(200).json(data);
    } catch (err) {
      next(new ServerException(errorMessages.SERVER_ERROR));
    }
  },
};

module.exports = PositionController;
