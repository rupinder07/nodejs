const Application = require('../model/applications');

const PositionService = {
  list: async () => {
    return Application.find({})
      .populate('position')
      .populate('user', { email: 1, username: 1 });
  },
  getById: async id => {
    return Application.findById(id)
      .populate('position')
      .populate('user', { email: 1, username: 1 });
  },
  create: async payload => {
    const jobApplication = await Application.create(payload);
    return jobApplication.save();
  },
};

module.exports = PositionService;
