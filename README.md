# Node JS API backend 

### Problem statement
Create an employee portal where the employees can apply for the available opening positions in the company. Also, the project managers can add an opening for the project.
A position needs to have the following values:
- Project Name
- Client Name 
- Technologies
- Role
- Job Description
- Status (open/closed)
- Created by
Status will determine if the position is visible to the employees of the company or not. A project manager will update the position state when the position is no longer available.
The portal will support 2 kinds of users:
Project Managers
Employees

# Application Setup
```sh
cp env.example .env
docker-compose build;
docker-compose up
```

## Application Sanity Test

- Application container port mapped with 3010 port of host
- check http://localhost:3010/api/v1/health

## Application Technical Details 

- Application Login/Register features usin Passport Auth
- Application also having social auth Login 
- Application will have by default user as employee or applicant (looking for a job)
- Application has Request validation and authneticatio and authoriation in place 
- Aplication is built with Express framework 
- Application has all neede middleware to authenticate and autorise user 
- Application has all Express APIs level security using modules 
- Application is using bcrypt module to create hash of password in mongo collection 
- Application has simple folder structure with all apis services managed in services Dir
- Application has upload feature added using mutler module, joi for request validation and custom error handler for managing api level errors 
- Application is using token based authentication which allows only logged In user to access protested apis if token is valid and passed in authorization header 


## Application Dependancies

- mongoose ODM 
- joi
- dotenv
- jsonwebtoken
- bcrypt-nodejs
- mutler
- passport
- passport-social-libs
- mocha
- nyc
- eslint 

## Application APIs exposded with swagger

* Logged in user can access all these APIs 
* Role - ['manager', 'employee']
* Manager can do Update, Delete or create new Position 
* Employee can read position and apply for it 

## api-spec for Applications, Applicant(Employee) and Position Entities

- /api/v1/health
- /api/v1/auth/login & /api/v1/auth/register & Google + Facebook Auth
- /api/v1/user GET & /api/v1/user/:userId PUT & GET + update role to become manager
- /api/v1/position & /api/v1/position/:userId CRUD + create, update and delete positions
- /api/v1/application &  /api/v1/application/apply/:positionId  