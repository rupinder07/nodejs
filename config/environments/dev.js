const configuration = {};
configuration.URL = {
  frontEnd: process.env.FE_URL,
};
configuration.url = {
  FE: process.env.FE,
  API: process.env.API,
};
configuration.logLevel = 'info';

module.exports = configuration;
